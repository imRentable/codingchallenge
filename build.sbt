name := "codingchallenge"

version := "0.1"

scalaVersion := "2.13.0"

libraryDependencies +=  "org.scalaj" %% "scalaj-http" % "2.4.2"
libraryDependencies += "junit" % "junit" % "4.13-beta-1"
libraryDependencies += "org.typelevel" %% "cats-core" % "2.0.0-RC1"

