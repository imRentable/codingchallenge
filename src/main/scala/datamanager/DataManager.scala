package datamanager

import datamanager.DataManager.{Data, StatusMessage}

import scala.util.Try

trait DataManager {

   /**
    * @return If successful, the imported [[Data]].
    */
   def importData(): Try[Data]

   /**
    * @param data The data to export.
    * @return If successful, an info denoting the status after exporting the data. E.g. a response from a server.
    */
   def exportData(data: Data): Try[StatusMessage]

}

object DataManager {
   type Data = String
   type StatusMessage = String
}
