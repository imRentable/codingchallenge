package datamanager.impl

import datamanager.DataManager
import datamanager.DataManager.Data
import scalaj.http._

import scala.util.Try

/**
 * Imports and exports [[Data]] from and to a HTTP interface.
 *
 * @param host      Hostname + port number.
 * @param lineCount The amount of lines that shall be imported and exported.
 */
class HttpDataManager(host: String, lineCount: Int) extends DataManager {

   private val connectionTimeoutMs = 10000

   // The default read timeout is pretty low which causes timeouts when exporting larger amounts of data.
   // Two minutes should suffice for most use cases.
   private val readTimeoutMs = 120000

   override def importData(): Try[Data] = Try {
      val response: HttpResponse[String] = Http(s"http://$host/articles/$lineCount").asString
      response.body
   }

   override def exportData(data: Data): Try[String] = Try {
      val resultResponse = Http(s"http://$host/products/$lineCount")
         .put(data)
         .header("content-type", "text/csv")
         .option(HttpOptions.connTimeout(connectionTimeoutMs))
         .option(HttpOptions.readTimeout(readTimeoutMs))
      resultResponse.asString.body.toString
   }
}
