package datamanager.impl

import datamanager.DataManager
import datamanager.DataManager.{Data, StatusMessage}

import scala.io.Source
import scala.util.{Success, Try}

/**
 * May be used for benchmarking [[app.App]] with large sets of data since the [[HttpDataManager]] will take while if the line count is very high.
 */
class FileDataManager(path: String) extends DataManager {
   override def importData(): Try[Data] = Try(Source.fromFile(path).mkString)

   override def exportData(data: Data): Try[StatusMessage] = Success("Data sent successfully.")
}
