
import app.App
import app.App._
import datamanager.DataManager
import datamanager.impl.HttpDataManager
import lineprocessor.LineProcessor
import lineprocessor.impl.DefaultLineProcessor

object Main {

   val dataManagerFactory: Args => DataManager = args => new HttpDataManager(args.host, args.lineCount)
   val lineProcessor: LineProcessor = DefaultLineProcessor
   val app: App = new App(dataManagerFactory, lineProcessor)

   def main(args: Array[String]): Unit = {

      println("Working on it ...")

      app.execute(args) match {
         case Left(appError) =>
            System.err.println(App.interpretAppError(appError))
            System.exit(-1)
         case Right(statusMessage) =>
            println(statusMessage)
            System.exit(0)
      }

   }

}