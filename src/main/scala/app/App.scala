package app

import app.App.AppError._
import app.App.Args
import cats.implicits._
import datamanager.DataManager
import datamanager.DataManager.{Data, StatusMessage}
import lineprocessor.LineProcessor
import lineprocessor.LineProcessor.LineParseError

import scala.util.Try

class App(dataManagerFactory: Args => DataManager, lineProcessor: LineProcessor) {

   import App._

   /**
    * Imports data from a [[DataManager]] created via [[dataManagerFactory]], processes the resulting lines via [[lineProcessor]] and
    * exports the processed data via the same [[DataManager]].
    *
    * @param args Arguments that will be parsed and fed to the [[DataManager]].
    * @return If successful, a [[StatusMessage]] denoting the status after exporting data via the [[DataManager]]. Else, an [[AppError]] denoting the
    *         occurred problem.
    */
   def execute(args: Array[String]): AppErrorOr[StatusMessage] = for {
      parsedArgs <- parseArgs(args)
      dataManager = dataManagerFactory(parsedArgs)
      data <- dataManager.importData().toEither.leftMap(DataImportError.apply)
      lines <- linesFromData(data)
      linesWithRemovedHeader <- removeHeader(lines)
      result <- lineProcessor.processLines(linesWithRemovedHeader).leftMap(LineProcessingError.apply)
      statusMsg <- dataManager.exportData(result).toEither.leftMap(DataExportError.apply)
   } yield statusMsg

}

object App {

   type AppErrorOr[A] = Either[AppError, A]

   /**
    * Container for the given program arguments. Used to construct a [[DataManager]].
    */
   case class Args(host: String, lineCount: Int)

   private val argsHint = "Please specify:\n- <host> e.g. localhost:8080\n- <line count> as positive integer"

   sealed trait AppError

   object AppError {

      /**
       * Denotes that something went wrong during data import.
       */
      case class DataImportError(cause: Throwable) extends AppError

      /**
       * Denotes that something went wrong while transforming the [[Data]] blob to separate input lines.
       */
      case class LineGenerationError(cause: Throwable) extends AppError

      /**
       * Denotes that something went wrong while parsing input lines to [[domain.Article]s the [[LineProcessor]].
       *
       * @param causes [[LineParseError]]s for every line that could not be parsed.
       */
      case class LineProcessingError(causes: Vector[LineParseError]) extends AppError

      /**
       * Denotes that something went wrong during data export.
       */
      case class DataExportError(cause: Throwable) extends AppError

      /**
       * Denotes that no program arguments were given.
       */
      case object NoArgs extends AppError

      /**
       * Denotes that insufficient program arguments were given.
       */
      case object InsufficientArgs extends AppError

      /**
       * Denotes that the line count argument could not be parsed.
       */
      case class LineCountParseError(cause: Throwable) extends AppError

      /**
       * Denotes that the header line could not be removed because the list of input lines was empty.
       */
      case object EmptyInputError extends AppError

      /**
       * Denotes that the specified line count was a negative integer.
       */
      case object LineCountNegativeError extends AppError

   }

   /**
    * @return A printable/loggable error messages describing the concrete [[AppError]].
    */
   def interpretAppError(appError: AppError): String = appError match {
      case DataImportError(cause) =>
         s"Could not import data because: $cause"
      case LineGenerationError(cause) =>
         s"Could not generate line because: $cause"
      case LineProcessingError(causes) =>
         val parsingErrorMessages = causes.map(_.getMessage).mkString("\n")
         s"Could not process lines because:\n$parsingErrorMessages"
      case DataExportError(cause) =>
         s"Could not export data because: $cause"
      case NoArgs =>
         s"No program arguments specified.\n$argsHint"
      case InsufficientArgs =>
         s"Insufficient arguments specified.\n$argsHint"
      case LineCountParseError(cause) =>
         s"Could not parse line count argument because: $cause"
      case EmptyInputError =>
         "Could not remove header line because input was empty."
      case LineCountNegativeError =>
         "Line count argument must be positive."
   }

   private def linesFromData(data: Data): Either[LineGenerationError, List[String]] =
      Try(data.split("\n").toList).toEither.leftMap(LineGenerationError.apply)

   private def parseLineCount(lineCount: String): Either[LineCountParseError, Int] =
      Try(lineCount.toInt).toEither.leftMap(LineCountParseError.apply)

   private def ensurePositiveLineCount(parsedLineCount: Int): Either[LineCountNegativeError.type, Int] = {
      if (parsedLineCount < 0)
         Left(LineCountNegativeError)
      else
         Right(parsedLineCount)
   }

   private def parseArgs(args: Array[String]): AppErrorOr[Args] = args.toList match {
      case Nil =>
         Left(NoArgs)
      case _ :: Nil =>
         Left(InsufficientArgs)
      case host :: lineCount :: _ =>
         for {
            parsedLineCount <- parseLineCount(lineCount)
            positiveLiveCount <- ensurePositiveLineCount(parsedLineCount)
         } yield Args(host, positiveLiveCount)
   }

   private def removeHeader(linesWithHeader: List[String]): Either[EmptyInputError.type, List[String]] = linesWithHeader match {
      case Nil =>
         Left(EmptyInputError)
      case _ :: rest =>
         Right(rest)
   }

}
