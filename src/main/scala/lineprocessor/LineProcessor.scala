package lineprocessor

import datamanager.DataManager
import datamanager.DataManager.Data
import domain.Article
import lineprocessor.LineProcessor._

trait LineProcessor {

   /**
    * @return If successful, a [[Data]] blob that may be exported via [[DataManager]] resulting from the given input lines.
    *         Else, a collection of [[LineParseError]]s.
    */
   def processLines(lines: List[String]): Result

}

object LineProcessor {

   /**
    * This may be generated when parsing an input line to an [[Article]] has failed.
    *
    * @param line  The line that could not be parsed.
    * @param cause The underlying exception.
    */
   case class LineParseError(line: String, cause: Throwable) {
      def getMessage: String = s"Failed to parse line: $line because: $cause"
   }

   type Result = Either[Vector[LineParseError], Data]

}