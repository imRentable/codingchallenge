package lineprocessor.impl

import cats.implicits._
import domain.{Article, Product}
import lineprocessor.LineProcessor.{LineParseError, Result}
import lineprocessor.LineProcessor

import scala.util.Try

object DefaultLineProcessor extends LineProcessor {

   val destinationHeader: String = "produktId|name|beschreibung|preis|summeBestand\n"

   private[impl] def parseLine(line: String): Either[LineParseError, Article] = Try {
      val words = line.split('|')

      Article(
         id = words(0),
         productId = words(1),
         name = words(2),
         description = words(3),
         price = words(4).toFloat,
         stock = words(5).toInt
      )
   }.toEither
      .leftMap(t => LineParseError(line, t))

   private def productFromArticle(article: Article): Product = Product(
      article.productId,
      article.name,
      article.description,
      article.price,
      article.stock
   )

   private def determineCheapestProduct(oldProduct: Product, newProduct: Product) =
      List(oldProduct, newProduct).minBy(_.price)

   /**
    * @return A [[Product]] constructed from the given [[Product]]s containing the properties of the cheapest one
    *         and the stock sum of both [[Product]]s.
    *         If both [[Product]]s have identical [[Product.price]] values, we keep the properties of the older product
    *         since it comes first in the input list.
    */
   private[impl] def mergeProducts(oldProduct: Product, newProduct: Product): Product = {
      val priceIsIdentical = oldProduct.price == newProduct.price
      val cheapestProduct =
         if (priceIsIdentical)
            oldProduct
         else
            determineCheapestProduct(oldProduct, newProduct)
      val stockSum = oldProduct.stockSum + newProduct.stockSum

      Product(
         cheapestProduct.productId,
         cheapestProduct.name,
         cheapestProduct.description,
         cheapestProduct.price,
         stockSum
      )
   }

   /**
    * If the given [[Article]] belongs to a different [[domain.ProductId]] we add the current best [[Product]] to our
    * result and replace the current best with the new [[Product]] resulting from the given [[Article]]. Else, we
    * determine the new best [[Product]] and add to the given [[Acc]].
    */
   private def accumulateArticle(acc: Acc, article: Article): Unit = {
      val hasNewProductId = acc.bestOpt.forall(_.productId != article.productId)
      val newProduct: Product = productFromArticle(article)

      if (hasNewProductId) {
         acc.addBestToResult()
         acc.replaceBest(newProduct)
      } else {
         val mergedProduct: Product = acc.bestOpt.fold(newProduct)(mergeProducts(_, newProduct))
         acc.replaceBest(mergedProduct)
      }
   }

   /**
    * We don't need to consider articles that have no stock, therefore we skip them. Otherwise, we process it further.
    */
   private def processArticle(acc: Acc, article: Article): Unit = {
      val skipArticle = article.stock > 0
      if (skipArticle)
         accumulateArticle(acc, article)
   }

   override def processLines(lines: List[String]): Result = {
      val acc = new Acc
      lines.foreach(parseLine(_).fold(acc.addError, processArticle(acc, _)))
      acc.addBestToResult() // We need to add the last best product since the loop doesn't do it.

      if (acc.hasErrors)
         Left(acc.errors)
      else
         Right(destinationHeader + acc.result)
   }

}
