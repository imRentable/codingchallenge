package domain

case class Article(id: String, productId: ProductId, name: String, description: String, price: Float, stock: Int)
