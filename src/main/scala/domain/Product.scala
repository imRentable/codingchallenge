package domain

case class Product(productId: ProductId, name: String, description: String, price: Float, stockSum: Int)
