import java.text.{DecimalFormat, DecimalFormatSymbols}

package object util {

   /**
    * Formats the given price by the [[DecimalFormat]] pattern 0.00.
    *
    * @note This means that prices without decimal places are formatted to have two zeros after the decimal point.
    *       This is contrary to the challenge description where the formatting example "90.0" is given for values without decimal places.
    *       However, if the price is formatted this way, we receive an info from the HTTP interface that two zeros are expected.
    * @param price The price to format.
    * @return The formatted price.
    */
   def formatPrice(price: Float): String = {
      val df = {
         val decimalSymbols = DecimalFormatSymbols.getInstance
         decimalSymbols.setDecimalSeparator('.')
         val pattern = "0.00"
         new DecimalFormat(pattern, decimalSymbols)
      }

      df.format(price)
   }

}
