package util

import org.junit.Assert._
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * Contains tests for [[util.formatPrice]].
 */
@RunWith(classOf[JUnit4])
class FormatPriceTest {

   @Test
   def noDecimalPlaces(): Unit = {
      val price = 42f
      val expected = "42.00"

      val actual = formatPrice(price)

      assertEquals(expected, actual)
   }

   @Test
   def oneDecimalPlace(): Unit = {
      val price = 1.1f
      val expected = "1.10"

      val actual = formatPrice(price)

      assertEquals(expected, actual)
   }

   @Test
   def moreThanTwoDecimalPlaces(): Unit = {
      val price = 1.337f
      val expected = "1.34"

      val actual = formatPrice(price)

      assertEquals(expected, actual)
   }

}