package lineprocessor.impl

import domain.{Article, Product}
import org.junit.Assert._
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * Contains tests for [[DefaultLineProcessor]] implementation of [[lineprocessor.LineProcessor]].
 */
@RunWith(classOf[JUnit4])
class DefaultLineProcessorTest {

   import DefaultLineProcessorTest._

   /**
    * [[DefaultLineProcessor.processLines()]] should yield the expected result resulting from the given "regular" input.
    */
   @Test
   def testProcessLinesRegularInput(): Unit = {
      val inputLines = List(
         s"foo1Id|$fooProductId|foo1|This is foo1.|11.33|2",
         s"foo2Id|$fooProductId|foo2|This is foo2.|5.44|3",
         s"foo3Id|$fooProductId|foo3|This is foo3.|5.44|8",
         s"foo4Id|$fooProductId|foo4|This is foo4.|2.05|0",
         s"bar1Id|$barProductId|bar1|This is bar1.|8.0|0",
         s"bar2Id|$barProductId|bar2|This is bar2.|42.0|0",
         s"baz1Id|$bazProductId|baz1|This is baz1.|43.80|2"
      )
      val expectedOutputLine1 = "fooProductId|foo2|This is foo2.|5.44|13\n"
      val expectedOutputLine2 = "bazProductId|baz1|This is baz1.|43.80|2\n"
      val expectedResult = Right(
         Vector(DefaultLineProcessor.destinationHeader, expectedOutputLine1, expectedOutputLine2).mkString
      )

      val actualOutputLines = DefaultLineProcessor.processLines(inputLines)

      assertEquals(expectedResult, actualOutputLines)
   }

   /**
    * [[DefaultLineProcessor.processLines()]] should only yield the [[DefaultLineProcessor.destinationHeader]] when
    * given an empty input.
    */
   @Test
   def testProcessLinesEmptyInput(): Unit = {
      val expected = Right(DefaultLineProcessor.destinationHeader)

      val actual = DefaultLineProcessor.processLines(Nil)

      assertEquals(expected, actual)
   }

   /**
    * A given line should be successfully parsed into an [[Article]].
    */
   @Test
   def testParseLineSuccess(): Unit = {
      val line = s"foo1Id|$fooProductId|foo1|This is foo1.|11.33|2"
      val expected = Right(Article("foo1Id", fooProductId, "foo1", "This is foo1.", 11.33f, 2))

      val actual = DefaultLineProcessor.parseLine(line)

      assertEquals(expected, actual)
   }

   /**
    * [[DefaultLineProcessor.parseLine()]] should fail if the price is malformed.
    */
   @Test
   def testParseLineFailPrice(): Unit = {
      val line = s"foo1Id|$fooProductId|foo1|This is foo1.|Foo|2"

      val result = DefaultLineProcessor.parseLine(line)

      assertTrue(result.isLeft)
      assertTrue(result.left.exists(_.line == line))
   }

   /**
    * [[DefaultLineProcessor.parseLine()]] should fail if the stock count is malformed.
    */
   @Test
   def testParseLineFailStock(): Unit = {
      val line = s"foo1Id|$fooProductId|foo1|This is foo1.|11.33|Foo"

      val result = DefaultLineProcessor.parseLine(line)

      assertTrue(result.isLeft)
      assertTrue(result.left.exists(_.line == line))
   }

   /**
    * [[DefaultLineProcessor.mergeProducts()]] should yield the old [[Product]] with the correct stock sum if the older
    * [[Product]] is cheaper.
    */
   @Test
   def testMergeProductsOldIsCheaper(): Unit = {
      val oldProduct = oldProductFromPrice(1.0f)
      val newProduct = newProductFromPrice(2.0f)
      val expected = oldProduct.copy(stockSum = 5)

      val actual = DefaultLineProcessor.mergeProducts(oldProduct, newProduct)

      assertEquals(expected, actual)
   }

   /**
    * [[DefaultLineProcessor.mergeProducts()]] should yield the new [[Product]] with the correct stock sum if the newer
    * [[Product]] is cheaper.
    */
   @Test
   def testMergeProductsNewIsCheaper(): Unit = {
      val oldProduct = oldProductFromPrice(2.0f)
      val newProduct = newProductFromPrice(1.0f)
      val expected = newProduct.copy(stockSum = 5)

      val actual = DefaultLineProcessor.mergeProducts(oldProduct, newProduct)

      assertEquals(expected, actual)
   }

   /**
    * [[DefaultLineProcessor.mergeProducts()]] should yield the old [[Product]] with the correct stock sum if the prices
    * are identical.
    */
   @Test
   def testMergeProductsIdenticalPrice(): Unit = {
      val oldProduct = oldProductFromPrice(1.0f)
      val newProduct = newProductFromPrice(1.0f)
      val expected = oldProduct.copy(stockSum = 5)

      val actual = DefaultLineProcessor.mergeProducts(oldProduct, newProduct)

      assertEquals(expected, actual)
   }

}

private object DefaultLineProcessorTest {

   def oldProductFromPrice(price: Float) = Product(fooProductId, "foo1", "This is foo1.", price, 2)
   def newProductFromPrice(price: Float) = Product(fooProductId, "foo2", "This is foo2.", price, 3)

   val sourceHeader: String = "id|produktId|name|beschreibung|preis|bestand"

   val fooProductId = "fooProductId"
   val barProductId = "barProductId"
   val bazProductId = "bazProductId"

}
